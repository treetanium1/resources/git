#!/usr/bin/bash

die () { echo "ERROR in ${BASH_SOURCE[0]}: ${1:-unknown}" >&2; exit 1; }

UTILDIR="${UTILDIR:-"$(realpath ./util)"}"
[[ ! -d "$UTILDIR" ]] && die "can't find utilies \$UTILDIR=${UTILDIR}"

set -e
. "${UTILDIR}/bash/getopts.sh"
. "${UTILDIR}/yq.sh" && ensure_yq
set +e
. "${UTILDIR}/bash/formatting.sh"


function gitsm_cfg_get () {
    local msg_usage="Usage: ${FUNC_NAME[0]} <sm path> <var>"
    git config -f .gitmodules "submodule.${1}.${2}" || \
        echo "Couldn't find SM \"${1}\" setting ${2}" >&2
}


function gitsm_current_revision  () {
    # print submodules' currently checked out revision (short commit id)
    git submodule status | awk '{gsub("[^a-z0-9]","",$1); print $2, substr($1,1,8)}'
}


function gitsm_files_changed () {
    # Print files changed since revisions, passed as arguments

    local -a args=("$@") gitsm_fc
    local i=0 verbose=false
    [[ "$1" == '-v' ]] && { verbose=true; shift; }
    while read -r d; do
        [[ "$verbose" == 'true' ]] && echo "* submodule ${d}: diff since ${args[$i]}"
        [[ -z "${args[$i]}" ]] && continue
        mapfile -t gitsm_fc < <(
            # start with processed files
            [[ -n "${gitsm_fc[*]}" ]] && printf "%s\n" "${gitsm_fc[@]}"
            # add current files
            cd "$d" && \
                git diff --name-only "${args[$i]}" 2>/dev/null | \
                sed "s|^|${d}/|"
        )
        ((i++))
    done <<< "$(git submodule status | awk '{print $2}')"

    printf "%s\n" "${gitsm_fc[@]}"
}


function gitsm_ci_access () {
    # Set up access to submodules: use access tokens in HTTP URLs
    local msg_usage="Usage: ${FUNCNAME[0]} [<git sm path>]
Ensure repository access by ensuring HTTPS URLs and injecting access tokens for private repos.
Run against all submodules if no path is passed as argument."

    local -a targets
    for arg in "$@"; do
        shift
        case $arg in
            -h|--help)
                echo -e "$msg_usage"; return 0;;
            *)
                [[ -d "$arg" ]] && targets+=("$arg") || set -- "$@" "$arg" ;;
        esac
    done

    . "${UTILDIR:-./util}/yq.sh" || \
        { echo "ERROR: can't find utils in \$UTILDIR='${UTILDIR}'" >&2; return 1; }
    ensure_yq || \
        { echo "${F_ERROR}ERROR: yq YAML parser not found${F_RESET}" >&2; return 1; }

    GIT_USER_DEFAULT=$CI_GIT_USER # NOTE: set in .gitlab-ci.yml
    GIT_TOKEN_DEFAULT=CI_GIT_TOKEN # NOTE: `$` omitted on purpose

    # Configure Git to use credential helper script to provide credentials
    # stored in env. var.s through CI/CD

    # NOTE: I did not get this to work, the user/token were not read properly
    # git config credential.helper "/bin/bash credential-helper.sh"

    # see done: pass dirs as arguments or run against all submodules if omitted
    while read -r sm_path; do
        echo "* submodule '${sm_path}'"

        url_old="$(gitsm_cfg_get "$sm_path" url)"
        [[ -z "$url_old" ]] && \
            { echo "WARNING: '${sm_path}.url' undefined. Skipping."; continue; }
        branch_old="$(gitsm_cfg_get "$sm_path" branch 2>/dev/null)"
        url_new="$url_old"
        echo "** old URL: ${url_old}"

        # Ensure HTTPS URL for SM - CI setting GIT_SUBMODULE_FORCE_HTTPS doesn't apply
        if [[ "$url_old" =~ ^git@ ]]; then
            url_http="${url_old#git@}"
            url_http="https://${url_http//:/\/}"
            url_new="$url_http"
            echo "** HTTP URL: ${url_new}"
        fi

        # Public repo's don't require credentials. Assume private by default here.
        if ! [[ $(yqd ".${sm_path}.private" "$GITSM_CFG") == 'false' ]]; then
            # Set credentials
            yqd ".$sm_path.user" "$GITSM_CFG" &>/dev/null && \
                GIT_USER="$(yqd ".$sm_path.user" "$GITSM_CFG")" || \
                GIT_USER="$GIT_USER_DEFAULT"
            yqd ".$sm_path.token" "$GITSM_CFG" &>/dev/null && \
                TOKEN_NAME="$(yqd ".$sm_path.token" "$GITSM_CFG")" || \
                TOKEN_NAME="$GIT_TOKEN_DEFAULT"

            # Add credentials to URL
            url_credentials="${url_new#https://}"
            # NOTE: using bash var. expansion `!` to insert var. value and get
            # it's value again
            url_credentials="https://$GIT_USER:${!TOKEN_NAME}@${url_credentials}"
            url_new="$url_credentials"
            [[ "$QUIET" != 'true' ]] && echo "** credential URL: $url_new"
        fi

        # Update SM URLs
        if [[ "$url_old" != "$url_new" ]] && [[ "$DRY" != 'true' ]]; then
            echo "** updating URL: $url_old -> $url_new"
            git config -f .gitmodules "submodule.${sm_path}.url" "$url_new"
        fi

        # update branch if mismatch
        branch_new="$(yqd ".$sm_path.branch" "$GITSM_CFG")"
        if [[ "$branch_new" != 'null' && "$branch_old" != "$branch_new" ]] && \
                [[ "$DRY" != 'true' ]]; then
            echo "** switching branch ${branch_old} -> ${branch_new}"
            git config -f .gitmodules "submodule.${sm_path}.branch" "${branch_new}"
        fi
    done <<< "${targets:-$(git submodule status | awk '{print $2}')}"

    # sync config (mostly URL) from .gitmodules to submodules' config
    git submodule sync >/dev/null
}


function gitsm_init_sparseCheckout () {
    # Sparse checkout for certain submodules to avoid pulling entire repos
    local msg_usage="Usage: ${FUNCNAME[0]} [<git sm path>]
source this file and call this function from superrepo as"

    if grep -q -v 'sparse-checkout' "$GITSM_CFG"; then
        echo "* nothing to do"
        return
    fi

    local -a targets
    for arg in "$@"; do
        shift
        case $arg in
            -h|--help)
                echo -e "$msg_usage"; return 0;;
            *)
                [[ -d "$arg" ]] && targets+=("$arg") || set -- "$@" "$arg" ;;
        esac
    done

    . "${UTILDIR:-./util}/yq.sh" || \
        { echo "ERROR: can't find utils in \$UTILDIR='${UTILDIR}'"; return 1; }
    ensure_yq # required YAML parser

    # see done: pass dirs as arguments or run against all submodules if omitted
    while read -r sm_path; do
        [[ ! -d "$sm_path" ]] && { echo "Directory ${sm_path} not found - skipping."; continue; }

        if yqd ".${sm_path}.sparse-checkout" "$GITSM_CFG" &>/dev/null; then
            echo "* submodule '${sm_path}'"

            # Git only creates submodules' config dir. when updating/cloning SMs
            local sm_gitdir=".git/modules/${sm_path}"
            [[ ! -d "$sm_gitdir" ]] && mkdir -p "$sm_gitdir" >/dev/null

            local url branch
            url="$(git config -f .gitmodules --get "submodule.${sm_path}.url")"
            branch="$(git config -f .gitmodules --get --default "origin/HEAD" "submodule.${sm_path}.branch")"
            local -a files
            mapfile -t files < <(yqd ".${sm_path}.sparse-checkout" "$GITSM_CFG" | sed 's/^- //')

            # if already initialised, clean for reinit.
            rm -rf "${sm_path:?}"/* "${sm_path:?}/.git" "$sm_gitdir" || true
            # BUG: this errored in an attempted migration by Git and doesn't remove .git/modules/<sm path>
            # git submodule deinit -f "$sm_path"

            # initialise submodule as regular Git repo
            # NOTE: Didn't find way of initialising and configuring a SM with less manual labour.
            # TODO: try this https://stackoverflow.com/a/63786181/16096134
            (
                cd "$sm_path" || exit
                git init 2>/dev/null
                git remote add origin "$url"
                git config core.sparseCheckout true
                printf "%s\n" "${files[@]}" > .git/info/sparse-checkout
                # NOTE: The Git way - haven't tested this!
                # git sparse-checkout set --no-cone "!/*" "${files[@]}"

                git fetch -q
                # TODO: preferrably check out/clone/pull with git submodule update later
                # on, but Git complains about not being able to find current revision (since
                # refs/HEAD were only set to branch, no commit)
                git checkout "$branch" 2>/dev/null
            ) >/dev/null && \
            {
                # manually turn Git repo into submodule
                mv "${sm_path}/.git" "$sm_gitdir"
                # this is how Git specifies path to .git dir in submodules
                echo "gitdir: ../../${sm_gitdir}" > "${sm_path}/.git"
            } || { echo "! failed to set up ${sm_path}"; continue; }
        else
            if [[ "$(git config --get "submodule.${sm_path}.core.sparseCheckout")" == 'true' ]]; then
                echo "** disabling sparse checkout for '${sm_path}'"
                (
                    cd "$sm_path"
                    git sparse-checkout disable >/dev/null
                )
            fi
        fi
    done <<< "${targets:-$(git submodule status | awk '{print $2}')}"
}


function gitsm_checkout_defaultBranch () {
    # Ensure HEADs aren't detached by checking out respective default branches.
    # Try value in our Git SM config, then .gitmodules, default to HEAD (not
    # necessarily a branch).
    while read -r line; do
        path="$(awk '{print $2}' <<< "$line")"
        branch="$(yqd ".${path}.branch" "$GITSM_CFG")" || \
            branch="$(git config --get -f .gitmodules "submodule.${path}.branch")" || \
            branch=HEAD
        ( cd "$path" && git checkout "$branch" )
    done <<< "${@:-"$(git submodule status)"}"
}


function gitsm_update () {
    # initilise + update/pull/clone Git SMs
    # optional argument: SM paths to be initialised
    # TODO: make this idempotent and usable to run it for updates and sync'ing config

    parse_options "$@"
    shift $((OPTIND - 1))
    GITSM_CFG="${CONFIG:-"$(realpath gitsm.yml)"}"
    # TODO: decide default behaviour if no config is present?
    # - default access token name
    # - no sparse checkout
    # - (init + ) update all submodules
    # - skip checking out default branches
    [[ ! -f "$GITSM_CFG" ]] && \
        { echo "ERROR: couldn't find Git SM config \$GITSM_CFG=${GITSM_CFG}"; return 1; }

    cat << EOF
${F_BOLD}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${F_RESET}
EOF

    # for CI/CD access to private repos has to be ensured,
    # for local use assuming SSH access in user's responsibility
    if [[ "$CI" == 'true' ]] ; then
        echo "Setting up private repo access ..."
        gitsm_ci_access
    fi

    echo "Setting up sparse checkout ..."
    gitsm_init_sparseCheckout

    # update/pull/clone submodules
    echo "Updating submodules"
    while read -r sm; do
        path="$(awk '{print $2}' <<< "$sm")"
        # Git reports `<commit> <path> (<head>)`
        head="$(awk '{print $3}' <<< "$sm" | sed 's/[()]//g')"
        # git submodule status returns empty for un-init. submodules
        if [[ -z "$head" ]]; then
            git submodule update --depth 1 --recommend-shallow --init --remote "$path"
            echo "* initialized ${path}"
        else
            git submodule update --remote "$path"
            echo "* updated ${path}"
        fi
    done <<< "$(git submodule status)"

    # Fix submodules' detached HEAD: `submodule update --remote` checks out a
    # commit, but we want to work on a branch.
    # In cicd this script is run every time and we don't want to check out a
    # branch, else we'd have to pull again.
    if [[ "$CI" != 'true' ]] ; then
        echo "Checking out default branch(s)"
        gitsm_checkout_defaultBranch "" &>/dev/null
    fi
}


if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
    gitsm_update "$@"
fi
