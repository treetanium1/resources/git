#!/usr/bin/env bash
git submodule checkout --init --recursive
# NOTE: $toplevel is set by Git to the super repo this command is called from
git submodule foreach 'git checkout $(git config --file $toplevel/.gitmodules --get submodule.$name.branch || echo master)'
